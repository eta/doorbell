#!/bin/bash

UNLOCKED_URL="https://doorbell.i.eta.st/unlocked"
USER_URL="https://doorbell.i.eta.st/"
PUSHOVER_TOKEN=""
PUSHOVER_USER_TOKEN=""
OPENJABNAB_SN=""
OPENJABNAB_TOKEN=""
TIME=90
CURTIME=$TIME

agi () {
	echo "$*"
	read
}

pushover () {
	curl -s \
		--form-string "token=$PUSHOVER_TOKEN" \
		--form-string "user=$PUSHOVER_USER_TOKEN" \
		--form-string "url=$USER_URL" \
		--form-string "message=$1" \
		--form-string "sound=$3" \
		--form-string "priority=${2:-0}" \
		--form-string "title=Doorbell" \
		-m 5 \
		https://api.pushover.net/1/messages.json >/dev/null 2>&1
}

nabaztag_tts () {
	curl -s -G \
		-d "sn=$OPENJABNAB_SN" \
		-d "token=$OPENJABNAB_TOKEN" \
		--data-urlencode "tts=$*" \
		-m 5 \
		http://openjabnab.fr/ojn/FR/api.jsp >/dev/null 2>&1
}

unlock_door () {
	agi "VERBOSE Unlocking door..."
	agi "EXEC Playback access-granted"
	agi "EXEC SendDTMF * 250 500"
	pushover "Door unlocked."
	nabaztag_tts "Door unlocked."
	sleep 60
	agi "HANGUP"
	exit 0
}


# Read all the headers and stuff that it gives you
while read line; do
	# Break out of the loop when we have an empty line (end of the headers)
	[ -z "$line" ] && break;
done

# Answer the call
agi "ANSWER"
# Send some audio to get the doorbell unit to accept our DTMF commands
# (if you just send DTMF, it won't accept it; there needs to be audio)
agi "SET MUSIC on default"
sleep 2
# Open the audio channel
agi "EXEC SendDTMF 0 250 500"
# Turn on nonblocking hold music while we wait
agi "SET MUSIC on default"

while [ "$CURTIME" -gt "0" ];
do
	if curl --max-time 1 --fail "$UNLOCKED_URL" >/dev/null 2>&1; then
		unlock_door
	fi
	if [ "$CURTIME" -eq "$TIME" ]; then
		# Tell the user to wait
		agi "EXEC Playback wait-moment"
		agi "SET MUSIC on default"
		pushover "Someone is at the door!" 1 "echo"
		nabaztag_tts "Someone is at the door!"
	fi
	sleep 0.5
	CURTIME=$((CURTIME - 1))
done

# Unlock the door
agi "EXEC Playback access-denied"
agi "HANGUP"
exit 400
