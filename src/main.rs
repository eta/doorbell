use handlebars::Handlebars;
use include_dir::{include_dir, Dir};
use lazy_static::lazy_static;
use serde::Serialize;
use std::sync::Mutex;
use time::format_description::OwnedFormatItem;
use time::{format_description, OffsetDateTime};

use crate::config::Config;
use crate::oidc::OidcClient;
use rouille::{router, Response};

mod config;
mod oidc;

static STATIC_DIR: Dir = include_dir!("$CARGO_MANIFEST_DIR/src/static");

lazy_static! {
    static ref FORMAT: OwnedFormatItem =
        format_description::parse_owned::<2>("[hour repr:24]:[minute]").unwrap();
}

#[derive(Debug)]
enum DoorState {
    Locked {
        last_unlocked_by: Option<String>,
        last_unlocked_at: Option<OffsetDateTime>,
    },
    Unlocked {
        at: OffsetDateTime,
        until: OffsetDateTime,
        by_username: String,
    },
}

impl Default for DoorState {
    fn default() -> Self {
        DoorState::Locked {
            last_unlocked_by: None,
            last_unlocked_at: None,
        }
    }
}

impl DoorState {
    pub fn unlock_door(&mut self, config: &Config, username: impl Into<String>) {
        let until = match self {
            DoorState::Locked { .. } => {
                let at = OffsetDateTime::now_utc();
                let until = at + config.unlock_duration();
                *self = DoorState::Unlocked {
                    at,
                    until,
                    by_username: username.into(),
                };
                until
            }
            DoorState::Unlocked {
                ref mut until,
                ref mut by_username,
                ..
            } => {
                *until += config.unlock_duration();
                *by_username = username.into();
                *until
            }
        };

        println!("Door unlocked until {}", until.format(&FORMAT).unwrap())
    }

    pub fn lock_door(&mut self) {
        if let DoorState::Unlocked {
            at, by_username, ..
        } = self
        {
            *self = DoorState::Locked {
                last_unlocked_by: Some(by_username.clone()),
                last_unlocked_at: Some(*at),
            }
        };
        println!("Door locked");
    }

    fn check_unlocked(&mut self) -> bool {
        match self {
            DoorState::Locked { .. } => false,
            DoorState::Unlocked { until, .. } => {
                if *until <= OffsetDateTime::now_utc() {
                    self.lock_door();
                    false
                } else {
                    true
                }
            }
        }
    }

    fn was_unlocked(&self) -> bool {
        match self {
            DoorState::Locked {
                last_unlocked_by,
                last_unlocked_at,
            } => last_unlocked_by.is_some() | last_unlocked_at.is_some(),
            DoorState::Unlocked { .. } => true,
        }
    }
}

#[derive(Serialize)]
struct HomeContext {
    username: String,
    unlocked: bool,
    unlocked_until: String,
    was_unlocked: bool,
    last_unlocked: String,
    last_unlocker_username: String,
}

impl HomeContext {
    pub fn new(username: String, state: &mut DoorState) -> Self {
        Self {
            username,
            unlocked: state.check_unlocked(),
            unlocked_until: if let DoorState::Unlocked { until, .. } = state {
                until.format(&FORMAT).unwrap()
            } else {
                "UNKNOWN".into()
            },
            was_unlocked: state.was_unlocked(),
            last_unlocked: match state {
                DoorState::Locked {
                    last_unlocked_at: Some(last_unlocked_at),
                    ..
                } => Some(last_unlocked_at),
                DoorState::Unlocked { at, .. } => Some(at),
                _ => None,
            }
            .map(|t| t.format(&FORMAT).unwrap())
            .unwrap_or_else(|| "UNKNOWN".into()),
            last_unlocker_username: match state {
                DoorState::Locked {
                    last_unlocked_by, ..
                } => last_unlocked_by.clone(),
                DoorState::Unlocked { by_username, .. } => Some(by_username.clone()),
            }
            .unwrap_or_else(|| "UNKNOWN".into()),
        }
    }
}

fn main() -> anyhow::Result<()> {
    let config_location = std::env::args()
        .nth(1)
        .unwrap_or_else(|| "./config.toml".into());
    println!("loading config at {config_location}...");

    let config: Config = toml::from_str(&std::fs::read_to_string(&config_location)?)?;

    let state = Mutex::new(DoorState::default());

    println!("setting up OIDC provider...");
    let oidc = OidcClient::new(
        config.oidc.issuer.clone(),
        config.oidc.client_id.clone(),
        config.oidc.secret.clone(),
        format!("{}/auth", config.baseurl),
        config.required_group.clone(),
    )?;

    let mut tpl = Handlebars::new();
    tpl.register_template_string("home", include_str!("./home.html.tpl"))?;

    println!("starting server on '{}'...", config.listen);

    rouille::start_server(config.listen.clone(), move |request| {
        macro_rules! require_auth {
            ($($body:tt)*) => {
                if oidc.is_authenticated(request) {
                    $($body)*
                } else {
                    Response::redirect_307("/login")
                }
            };
        }

        if request.method() == "GET" {
            if let Some(path) = request.url().strip_prefix("/static/") {
                if let Some(f) = STATIC_DIR.get_file(path) {
                    return Response::from_data(
                        match mime_guess::from_path(path).first() {
                            Some(mime) => mime.to_string(),
                            None => "text/plain".into(),
                        },
                        f.contents_utf8().unwrap(),
                    );
                }
            }
        }

        router!(request,
                (GET) (/) => {
                    require_auth! {
                        Response::html(
                            tpl.render("home", &HomeContext::new(
                                oidc.get_username(request).unwrap(),
                                &mut state.lock().unwrap())).unwrap()
                        )
                    }
                },
                (POST) (/unlock-door) => {
                    require_auth! {
                        state.lock().unwrap().unlock_door(&config, oidc.get_username(request).unwrap());
                        Response::redirect_301("/")
                    }
                },
                (POST) (/lock-door) => {
                    require_auth! {
                        state.lock().unwrap().lock_door();
                        Response::redirect_301("/")
                    }
                },
                (GET) (/unlocked) => {
                    if state.lock().unwrap().check_unlocked() {
                        Response::text("Door is unlocked")
                    } else {
                        Response::text("Door is locked").with_status_code(400)
                    }
                },
                (GET) (/login) => {
                    match oidc.generate_redirect_url() {
                        Ok(v) => Response::redirect_302(v),
                        Err(e) => {
                            eprintln!("failed to generate OIDC redirect! {e:?}");
                            Response::text("whoops, looks like I'm broken")
                                .with_status_code(500)
                        }
                    }
                },
                (GET) (/auth) => {
                    match oidc.handle_redirect(request) {
                        Ok(v) => v,
                        Err(e) => {
                            eprintln!("failed to handle OIDC redirect! {e:?}");
                            Response::text("whoops, looks like I'm broken")
                                .with_status_code(500)
                        }
                    }
                },
                _ => {
                    Response::empty_404()
                }
        )
    });
}
