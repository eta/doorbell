//! Configuration file.

use std::time::Duration;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct OidcConfig {
    pub client_id: String,
    pub secret: String,
    pub issuer: String,
}

#[derive(Deserialize)]
pub struct Config {
    pub oidc: OidcConfig,
    pub baseurl: String,
    pub listen: String,
    pub unlock_seconds: u64,
    #[serde(default)]
    pub required_group: Option<String>,
}
impl Config {
    pub(crate) fn unlock_duration(&self) -> Duration {
        Duration::from_secs(self.unlock_seconds)
    }
}
