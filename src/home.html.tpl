<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Door Zone</title>
  <link rel="stylesheet" href="static/style.css" />
</head>
<body>
  <main>
    <div class="contents">
      <header>
        <img src="static/eta-styled.svg" alt="eta" />
        <h1 class="cool-text-shadow">Door Zone</h1>
      </header>

      <h2>Hello, {{username}}</h2>

      {{ #if unlocked }}
      <p>Door unlocked until {{unlocked_until}}</p>
      <p>Dial 6 on the door pad, then press the call button to open the door</p>
      {{ else }}
      <p>Door locked</p>
      {{ /if }}

      {{ #if was_unlocked }}
      <p>Last unlocked at {{last_unlocked}} by {{last_unlocker_username}}</p>
      {{ /if }}

      <form method="post">
      {{ #if unlocked }}
        <input type="submit" value="Lock now" formaction="/lock-door" />
      {{ /if }}
        <input type="submit" value="Unlock" formaction="/unlock-door" />
      </form>
    </div>
  </main>
</body>
</html>
